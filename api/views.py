from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def jumlah(request):
    x = request.GET['x']
    y = request.GET['y']

    response = int(x) + int(y)
    return HttpResponse("Hasil Jumlah : " + str(response))